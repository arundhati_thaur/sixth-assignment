package com.example.arundhati.touchtoggle.utils;

/**
 * Created by Arundhati on 10/13/2015.
 */
public interface AppConstants {
    int INSIDE_CIRCLE = 1;
    int ON_CIRCLE = 2;
    int OUTSIDE_CIRCLE = 3;
    int MIN_ROWS = 0;
    int MIN_COLUMNS = 0;
    int MAX_ROWS = 5;
    int MAX_COLUMNS = 5;
    String INSIDE_TEXT = "Inside Circle";
    String ON_CIRCLE_TEXT = "On Circle";
    String OUTSIDE_TEXT = "Outside Circle";
    String INVALID_INPUT = "Invalid Input";
    String TOAST_TEXT1 = "enter the no of rows and columns";
    String TOAST_TEXT2 = "Max limit for rows and columns is 5";
    String LEFT_SCREEN = "Left the Screen";

}
