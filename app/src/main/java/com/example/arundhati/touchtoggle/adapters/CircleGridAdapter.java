package com.example.arundhati.touchtoggle.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;

import com.example.arundhati.touchtoggle.R;

/**
 * Created by Arundhati on 10/13/2015.
 */
public class CircleGridAdapter extends BaseAdapter {
    Context context;
    int rows, cols,width, height;

    public CircleGridAdapter(Context context, int numRows, int numCols,float height,float width) {
        this.context = context;
        this.rows = numRows;
        this.cols = numCols;
        this.width= (int) (width/numCols);
        this.height= (int) (height/numRows);
    }


    @Override
    public int getCount() {
        return rows*cols;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (height < width)
            width = height;
        else if (width < height)
            height = width;
        AbsListView.LayoutParams circleParameter = new AbsListView.LayoutParams(width, height);
        LayoutInflater li = LayoutInflater.from(context);
        convertView = li.inflate(R.layout.circlrgrid_layout, null, false);
        convertView.setLayoutParams(circleParameter);
        return convertView;
    }
}
