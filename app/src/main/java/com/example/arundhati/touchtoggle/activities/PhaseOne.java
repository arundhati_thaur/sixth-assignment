
package com.example.arundhati.touchtoggle.activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.example.arundhati.touchtoggle.utils.AppConstants;
import com.example.arundhati.touchtoggle.R;

public class PhaseOne extends AppCompatActivity implements View.OnTouchListener, AppConstants, View.OnClickListener {
    View circleShape;
    RelativeLayout touchLayout;
    TextView output;
    Button nextPhase;
    Boolean flag = true;
    float radius, xCenter, yCenter;
    GradientDrawable backgroundColor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phase_one);
        initialization();
    }
    public void initialization()
    {
        output = (TextView) findViewById(R.id.outputText);
        nextPhase = (Button) findViewById(R.id.nextButton);
        touchLayout = (RelativeLayout) findViewById(R.id.touchLayout);
        circleShape = (View) findViewById(R.id.circle);
        backgroundColor = (GradientDrawable) circleShape.getBackground();
        touchLayout.setOnTouchListener(this);
        nextPhase.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intentObj = new Intent(this, PhaseTwo.class);
        startActivity(intentObj);
        finish();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        getCenterLocationOnScreen();
        switch (event.getAction()) {
            case MotionEvent.ACTION_UP:
                output.setText(LEFT_SCREEN);
                break;
            case MotionEvent.ACTION_DOWN:
                touchEvent(event.getX(), event.getY());
                break;
            case MotionEvent.ACTION_MOVE:
                touchEvent(event.getX(), event.getY());
                break;
        }
        return true;
    }

    public void toggleCircleColor(int position) {
        switch (position) {
            case INSIDE_CIRCLE:
                if(!(output.getText().toString().equalsIgnoreCase(INSIDE_TEXT))) {
                    output.setText(INSIDE_TEXT);

                    flag = !flag;
                }
                break;
            case ON_CIRCLE:
                backgroundColor.setColor(Color.GREEN);
                output.setText(ON_CIRCLE_TEXT);
                break;
            case OUTSIDE_CIRCLE:

                output.setText(OUTSIDE_TEXT);
                break;
        }
            if (flag) {
                backgroundColor.setColor(Color.YELLOW);
            } else {
                backgroundColor.setColor(Color.CYAN);

        }


    }
    public void touchEvent(float xCoordinate, float yCoordinate) {
        double distance;
        distance = calculateDistance(xCoordinate, yCoordinate);
        if (distance < radius) {
            toggleCircleColor(INSIDE_CIRCLE);
        } else if (distance == radius) {
            toggleCircleColor(ON_CIRCLE);
        } else {
            toggleCircleColor(OUTSIDE_CIRCLE);
        }
    }

    public double calculateDistance(float xCoordinate, float yCoordinate) {
        double distance = Math.sqrt(Math.pow((xCoordinate - xCenter), 2) + Math.pow((yCoordinate - yCenter), 2));
        return distance;
    }

    public void getCenterLocationOnScreen() {
        radius = (circleShape.getBottom() + circleShape.getTop()) / 2;
        xCenter = (circleShape.getRight() + circleShape.getLeft()) / 2;
        yCenter = (circleShape.getBottom() + circleShape.getTop()) / 2;

    }
}
