package com.example.arundhati.touchtoggle.activities;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.arundhati.touchtoggle.R;
import com.example.arundhati.touchtoggle.adapters.CircleGridAdapter;
import com.example.arundhati.touchtoggle.utils.AppConstants;

import java.util.Arrays;

public class PhaseThree extends AppCompatActivity implements View.OnClickListener,AppConstants, View.OnTouchListener {
    TextView output;
    EditText noOfRows,noOfColumns;
    Button submit;
    View circleShape;
    int rows,cols;
    GradientDrawable backgroundCircle;
    float radius,xCenter,yCenter;
    Boolean[] flag;
    GridView circleGrid;
    CircleGridAdapter circleGridAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phase_three);
        initialization();


    }
    public void initialization()
    {
        output = (TextView)findViewById(R.id.outputText);
        circleGrid = (GridView)findViewById(R.id.circleGrid);
        noOfRows = (EditText)findViewById(R.id.noOfRows);
        noOfColumns = (EditText)findViewById(R.id.noOfColumns);
        submit = (Button)findViewById(R.id.submitButton);
        submit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        hideKeybord();
        String strRows = noOfRows.getText().toString();
        String strCols = noOfColumns.getText().toString();
        if (strRows.isEmpty() || strCols.isEmpty()) {
            Toast.makeText(this,TOAST_TEXT1,Toast.LENGTH_SHORT).show();
            return;
        }
        rows = Integer.parseInt(strRows);
        cols = Integer.parseInt(strCols);
        if ((rows > MIN_ROWS && rows <= MAX_ROWS) && (cols > MIN_COLUMNS&& cols <= MAX_COLUMNS)) {
            flag = new Boolean[rows * cols];
            Arrays.fill(flag, false);
            float gridViewHeight = circleGrid.getHeight();
            float gridViewWidth = circleGrid.getWidth();
            circleGrid.setNumColumns(cols);
            circleGridAdapter = new CircleGridAdapter(this, rows, cols, gridViewHeight, gridViewWidth);
            circleGrid.setAdapter(circleGridAdapter);
            circleGridAdapter.notifyDataSetChanged();
            circleGrid.setOnTouchListener(this);
        } else {
            noOfRows.setText("");
            noOfColumns.setText("");
            output.setText(INVALID_INPUT);
            Toast.makeText(this,TOAST_TEXT2,Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        float currentXPosition = event.getX();
        float currentYPosition = event.getY();
        int position = circleGrid.pointToPosition((int) currentXPosition, (int) currentYPosition);
        if (position < 0) {
            output.setText(OUTSIDE_TEXT);
            return true;
        }
        calcCircleCenter(position);
        switch (event.getAction()) {
            case MotionEvent.ACTION_UP:
                output.setText(LEFT_SCREEN);

                break;
            case MotionEvent.ACTION_DOWN:
                touchEvent(event.getX(), event.getY(), position);

                break;
            case MotionEvent.ACTION_MOVE:
                touchEvent(event.getX(), event.getY(), position);
                break;
        }
        return true;
    }
    public void touchEvent(float currX, float currY, int position) {
        double distance;
        distance = calculateDistance(currX, currY);
        if (distance < radius) {
            toggleCircleColour(INSIDE_CIRCLE, position);
        } else if (distance == radius) {
            toggleCircleColour(ON_CIRCLE, position);
        } else {
            toggleCircleColour(OUTSIDE_CIRCLE, position);
        }
    }

    public void calcCircleCenter(int position) {
        circleShape = circleGrid.getChildAt(position);
        backgroundCircle = (GradientDrawable) circleShape.getBackground();
        radius = (circleShape.getWidth()) / 2;
        xCenter = (circleShape.getRight() + circleShape.getLeft()) / 2;
        yCenter = (circleShape.getBottom() + circleShape.getTop()) / 2;
    }

    public double calculateDistance(float xCoordinate, float yCoordinate) {
        double distance;
        distance = Math.sqrt(Math.pow((xCoordinate - xCenter), 2) + Math.pow((yCoordinate - yCenter), 2));
        return distance;
    }

    public void toggleCircleColour(int cursorAt, int position) {
        switch (cursorAt) {
            case INSIDE_CIRCLE:
                if (!(output.getText().toString().equalsIgnoreCase(INSIDE_TEXT + position))) {
                    output.setText(INSIDE_TEXT + position);
                    flag[position] = !flag[position];
                }
                break;
            case ON_CIRCLE:
                backgroundCircle.setColor(Color.RED);
                output.setText(ON_CIRCLE_TEXT + position);
                break;
            case OUTSIDE_CIRCLE:
                output.setText(OUTSIDE_TEXT);
                break;
        }
        if (flag[position])
            backgroundCircle.setColor(Color.BLUE);
        else
            backgroundCircle.setColor(Color.CYAN);
    }
    public void hideKeybord() {

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm.isActive())
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    }

}
