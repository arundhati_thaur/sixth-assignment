package com.example.arundhati.touchtoggle.activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

import com.example.arundhati.touchtoggle.utils.AppConstants;
import com.example.arundhati.touchtoggle.adapters.CircleGridAdapter;
import com.example.arundhati.touchtoggle.R;

import java.util.Arrays;

public class PhaseTwo extends AppCompatActivity implements View.OnTouchListener,AppConstants, View.OnClickListener {
    TextView output;
    View circleShape;
    Button nextPhase;
    GridView circleGrid;
    float radius, xCenter, yCenter;
    CircleGridAdapter circleGridAdapter;
    GradientDrawable circleBackground;
    Boolean[] flag;
    int rows = 2,cols = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phase_two);
        initialization();
    }
    public void initialization()
    {
        output = (TextView)findViewById(R.id.outputText);
        nextPhase = (Button) findViewById(R.id.nextButton);
        circleGrid = (GridView) findViewById(R.id.circle_grid);
        float gridViewHeight = 750, gridViewWidth = 550;
        flag = new Boolean[rows * cols];
        Arrays.fill(flag, false);
        circleGridAdapter = new CircleGridAdapter(this, rows, cols, gridViewHeight, gridViewWidth);
        circleGrid.setNumColumns(cols);
        circleGrid.setAdapter(circleGridAdapter);
        circleGridAdapter.notifyDataSetChanged();
        circleGrid.setOnTouchListener(this);
        nextPhase = (Button)findViewById(R.id.nextButton);
        nextPhase.setOnClickListener(this);

    }

    public void touchEvent(float currX, float currY, int position) {
        double distance;
        distance = calculateDistance(currX, currY);
        if (distance < radius) {
            toggleCircleColour(INSIDE_CIRCLE, position);
        } else if (distance == radius) {
            toggleCircleColour(ON_CIRCLE, position);
        } else {
            toggleCircleColour(OUTSIDE_CIRCLE, position);
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        float currentXPosition = event.getX();
        float currentYPosition = event.getY();
        int position = circleGrid.pointToPosition((int) currentXPosition, (int) currentYPosition);
        if (position < 0) {
            output.setText(OUTSIDE_TEXT);
            return true;
        }
        calcCircleCenter(position);
        switch (event.getAction()) {
            case MotionEvent.ACTION_UP:
                output.setText(LEFT_SCREEN);
                break;
            case MotionEvent.ACTION_DOWN:
                touchEvent(event.getX(), event.getY(), position);

                break;
            case MotionEvent.ACTION_MOVE:
                touchEvent(event.getX(), event.getY(), position);
                break;
        }
        return true;
    }

    public void calcCircleCenter(int position) {
        circleShape = circleGrid.getChildAt(position);
        circleBackground = (GradientDrawable) circleShape.getBackground();
        radius = (circleShape.getWidth()) / 2;
        xCenter = (circleShape.getRight() + circleShape.getLeft()) / 2;
        yCenter = (circleShape.getBottom() + circleShape.getTop()) / 2;
    }

    public double calculateDistance(float xCoordinate, float yCoordinate) {
        double distance;
        distance = Math.sqrt(Math.pow((xCoordinate - xCenter), 2) + Math.pow((yCoordinate - yCenter), 2));
        return distance;
    }

    public void toggleCircleColour(int cursorAt, int position) {
        switch (cursorAt) {
            case INSIDE_CIRCLE:
                if (!(output.getText().toString().equalsIgnoreCase(INSIDE_TEXT + position))) {
                    output.setText(INSIDE_TEXT + position);
                    flag[position] = !flag[position];
                }
                break;
            case ON_CIRCLE:
                circleBackground.setColor(Color.RED);
                output.setText(ON_CIRCLE_TEXT + position);
                break;
            case OUTSIDE_CIRCLE:
                output.setText(OUTSIDE_TEXT);
                break;
        }
        if (flag[position])
            circleBackground.setColor(Color.GREEN);
        else
            circleBackground.setColor(Color.CYAN);
    }

    @Override
    public void onClick(View v) {
        Intent intentObj = new Intent(this,PhaseThree.class);
        startActivity(intentObj);
        finish();
    }
}